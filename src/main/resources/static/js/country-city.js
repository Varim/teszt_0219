$(function(){

	$.get("/rest/countries", function (response) {
		$.each(response, (index, value) => {
			$('#city-country').append('<option>'+value.iso+'</option>')
		})
	})

	$('body').on('click', '#add-city-button', function(){
		var cityData = {
			"name" : $('#city-name').val(),
			"population" : $('#city-population').val(),
			"country.iso" : $('#city-country').val()
		};
		$.ajax({
			url: '/rest/cities',
			method : 'PUT',
			data : cityData,
			dataType : 'json',
			complete : function(){
				$('.close').trigger('click');
			}
		});
	});

	$('table').on('click', '.delete-country', function() {
			var iso = $(this).closest('tr').data('iso');
			var name = $(this).parent().siblings().children().first().text();
			if (confirm("Are you sure?")) {
				$.ajax({
					url: '/rest/countries/'+iso,
					method : 'DELETE',
					success : (() => {
						alert(name + " has been deleted")
					})
				})
				$(this).closest('tr').remove();
			}
	})

	$('table').on('click', '.delete-city',function()  {
			var id = $(this).closest('tr').data('id');
			var name = $(this).parent().siblings().first().text();
			if (confirm("Are you sure?")) {
				$.ajax({
					url: '/rest/cities/'+id,
					method : 'DELETE',
					success : (() => {
						alert(name + " has been deleted")
					})
				})
				$(this).closest('tr').remove();
			}
	})


});
